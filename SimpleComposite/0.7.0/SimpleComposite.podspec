Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "SimpleComposite"
s.summary = "SimpleComposite objective C implemeentation of the composite pattern."
s.requires_arc = true

# 2
s.version = "0.7.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 -
s.author = { "Didier Lobeau" => "didier.lobeau@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://gitlab.com/dlobeau/simplecompositeinobjectivec"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source ={ :git => 'https://gitlab.com/dlobeau/simplecompositeinobjectivec.git', :tag => '0.7.0' }

# 7
s.framework = "UIKit"

# 8
s.source_files = "SimpleComposite/**/*.{m,h}"
s.public_header_files = "SimpleComposite/**/*.h"
s.exclude_files = "SimpleComposite/**/Info.plist"

# 9



end

