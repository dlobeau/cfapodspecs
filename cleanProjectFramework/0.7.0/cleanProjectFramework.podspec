Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '10.0'
s.name = "cleanProjectFramework"
s.summary = "cleanProjectFramework framework for a clean IOS project in objective C"
s.requires_arc = true

# 2
s.version = "0.7.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 -
s.author = { "Didier Lobeau" => "didier.lobeau@gmail.com" }

# 5 - Replace this URL with your own GitHub page's URL (from the address bar)
s.homepage = "https://gitlab.com/dlobeau/clean-project-framework-objc"

# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source ={ :git => 'https://gitlab.com/dlobeau/clean-project-framework-objc.git', :tag => '0.7.0' }

# 7
s.framework = "UIKit"
s.dependency 'SimpleComposite', '~> 0.5.5'
# 8
s.source_files = "cleanProjectFramework/**/*.{m,h}"
s.public_header_files = "cleanProjectFramework/**/*.h"
s.exclude_files = "cleanProjectFramework/**/Info.plist"

# 9



end
